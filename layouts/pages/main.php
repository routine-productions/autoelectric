<div class="Left-Content">
    <?php require_once "./modules/main-menu.php"; ?>
</div>
<div class="Middle"></div>
<div class="Right-Content">
    <div class="Main-Wrap">
        <?php require_once "./modules/main-slider.php"; ?>
        <?php require_once "./modules/main-hits.php"; ?>
    </div>
</div>