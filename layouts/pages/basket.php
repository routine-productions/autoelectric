<div class="Left-Content">
    <section class="Latest">
        <h2>
            <svg>
                <use xlink:href="#eye"></use>
            </svg>
            Недавно просмотренные
        </h2>

        <?php
        for ($Index = 0; $Index < 3; $Index++) {
            require "./modules/item-in-basket.php";
        }
        ?>
    </section>
</div>
<div class="Middle"></div>

<div class="Right-Content Mod">
    <section class="Basket">

        <h2>
            <svg>
                <use xlink:href="#basket"></use>
            </svg>
            Корзина
        </h2>

        <div class="Basket-Next">Далее</div>
        <div class="Basket-Previous">Назад</div>

        <section class="Basket-Items Active">
            <ul>
                <li class="Basket-Item">
                    <span class="Basket-Number">01</span>

                    <div class="Basket-Picture">
                        <img src="/img/basket-item-1.jpg" alt="">
                    </div>
                    <span class="Basket-Name">HYUNDAI H-CA1200</span>
                     <span class="Basket-Price">210.00
                        <svg class="Rouble">
                            <use xlink:href="#rub"></use>
                        </svg>
                    </span>

                    <div class="Basket-Count">
                        <div class="Previous JS-Click-Left">
                            <svg>
                                <use xlink:href="#arrow"></use>
                            </svg>
                        </div>
                        <div class="Basket-Amount">
                            <span></span>
                        </div>
                        <div class="Next JS-Click-Right">
                            <svg>
                                <use xlink:href="#arrow"></use>
                            </svg>
                        </div>
                    </div>
                    <div class="Basket-Close">
                        <svg>
                            <use xlink:href="#cross"></use>
                        </svg>
                    </div>
                </li>
                <li class="Basket-Item">
                    <span class="Basket-Number">02</span>

                    <div class="Basket-Picture">
                        <img src="/img/basket-item-2.jpg" alt="">
                    </div>
                    <span class="Basket-Name">Prology iREG-6250GPS</span>
                    <span class="Basket-Price">3800.00
                         <svg class="Rouble">
                             <use xlink:href="#rub"></use>
                         </svg>
                    </span>

                    <div class="Basket-Count">
                        <div class="Previous JS-Click-Left">
                            <svg>
                                <use xlink:href="#arrow"></use>
                            </svg>
                        </div>
                        <div class="Basket-Amount">
                            <span></span>
                        </div>
                        <div class="Next JS-Click-Right">
                            <svg>
                                <use xlink:href="#arrow"></use>
                            </svg>
                        </div>
                    </div>
                    <div class="Basket-Close">
                        <svg>
                            <use xlink:href="#cross"></use>
                        </svg>
                    </div>
                </li>
                <li class="Basket-Item">
                    <span class="Basket-Number">03</span>

                    <div class="Basket-Picture">
                        <img src="/img/basket-item-3.jpg" alt="">
                    </div>
                    <span class="Basket-Name">Erisson VR-F105</span>
                    <span class="Basket-Price">4250.00
                         <svg class="Rouble">
                             <use xlink:href="#rub"></use>
                         </svg>
                    </span>

                    <div class="Basket-Count">
                        <div class="Previous JS-Click-Left">
                            <svg>
                                <use xlink:href="#arrow"></use>
                            </svg>
                        </div>
                        <div class="Basket-Amount">
                            <span></span>
                        </div>
                        <div class="Next JS-Click-Right">
                            <svg>
                                <use xlink:href="#arrow"></use>
                            </svg>
                        </div>
                    </div>
                    <div class="Basket-Close">
                        <svg>
                            <use xlink:href="#cross"></use>
                        </svg>
                    </div>
                </li>
            </ul>


            <span class="All-Price">Итого:
                <span class="JS-All-Price">8260.00</span>
                 <svg class="Rouble">
                     <use xlink:href="#rub"></use>
                 </svg>
            </span>
        </section>


        <section class="Basket-Method">
            <h2>Выберите способ доставки</h2>

            <ul>
                <li class="Method-Item">
                    <div class="Method-Choice">
                        <input type="radio" name="method" id="method-1" checked>
                        <svg>
                            <use xlink:href="#checkbox"></use>
                        </svg>
                        <label for="method-1"></label>
                    </div>
                    <span class="Method-Number">01</span>

                    <div class="Method-Description">
                        <h3>Доставка курьером по предоплате</h3>
                        <span>г. Владимир и Владимирская область<span> (100% предоплата)</span></span>
                    </div>
                    <div class="Method-Time">
                        <span>3-5 дней</span>
                    </div>
                </li>
                <li class="Method-Item">
                    <div class="Method-Choice">
                        <input type="radio" name="method" id="method-2">
                        <svg>
                            <use xlink:href="#checkbox"></use>
                        </svg>
                        <label for="method-2"></label>
                    </div>
                    <span class="Method-Number">02</span>

                    <div class="Method-Description">
                        <h3>Доставка Транспортной компанией</h3>
                        <span>вся Россия<span> (100% предоплата)</span></span>
                    </div>
                    <div class="Method-Time">
                        <span>3-5 дней</span>
                        <span class="Time-Plus">+ Сроки транспортной компании</span>
                    </div>
                </li>
                <li class="Method-Item Himself">
                    <div class="Method-Choice">
                        <input type="radio" name="method" id="method-3">
                        <svg>
                            <use xlink:href="#checkbox"></use>
                        </svg>
                        <label for="method-3"></label>
                    </div>
                    <span class="Method-Number">03</span>

                    <div class="Method-Description">
                        <h3>Самовывоз</h3>
                        <span>г. Владимир<span> (50% предоплата)</span></span>
                    </div>
                    <div class="Method-Time">
                        <span>3-5 дней</span>
                    </div>
                </li>
            </ul>
        </section>


        <section class="Basket-Contacts">
            <h2>Ваши контактные данные</h2>

            <form action="">
                <ul>
                    <li>
                        <label for="basket-fio">Ф.И.О.</label>
                        <input type="text" id="basket-fio">
                    </li>
                    <li>
                        <label for="basket-phone">Телефон</label>
                        <input type="text" id="basket-phone">
                    </li>
                    <li class="Basket-Address">
                        <label for="basket-address">Адрес доставки</label>
                        <input type="text" id="basket-address">
                    </li>
                    <li  class="Basket-Comment">
                        <label for="basket-comment">Комментарий к доставке</label>
                        <textarea id="basket-comment"></textarea>
                    </li>
                </ul>

                <button>Перейти к оплате</button>
            </form>
        </section>


    </section>
</div>




