<div class="Left-Content">
    <section class="Items-Similar">
        <h2>Похожие товары</h2>
        <?php
        for ($index = 0; $index < 3; $index++) {
            require "./modules/item.php";
        }
        ?>
    </section>
</div>
<div class="Middle"></div>
<div class="Right-Content Mog-bg">
    <nav class="Thing-Nav">
        <div class="Thing-Breadcrumbs">
            <a href="">

                <span>
                    <svg>
                        <use xlink:href="#back"></use>
                    </svg>вернуться в категорию <span>Наборы проводов</span></a>
            </span>
        </div>
        <div class="Thing-Price">
            <span>Цена:</span>
            <span>260.00
                <svg>
                    <use xlink:href="#rub"></use>
                </svg>
            </span>
        </div>
    </nav>

    <div class="Thing-Content">
        <div class="Thing-View"></div>
        <section class="Thing-Description">
            <h2>Hyundai H-CA2200</h2>
            <dl>
                <dd>Производитель:</dd>
                <dt>Hyundai</dt>
            </dl>
            <dl>
                <dd>Модель:</dd>
                <dt>HYUNDAI H-CA2200</dt>
            </dl>
            <dl>
                <dd>Наличие:</dd>
                <dt>Есть в наличии</dt>
            </dl>


            <button>В корзину</button>
        </section>
    </div>
    <section class="Thing-Info">
        <h3>Характеристики</h3>
        <dl>
            <dt>Производитель</dt>
            <dd>Fusion</dd>
        </dl>
        <dl>
            <dt>Модель</dt>
            <dd>F-AK 2-4</dd>
        </dl>
        <dl>
            <dt>Тип</dt>
            <dd>Набор проводов</dd>
        </dl>
        <h4>Дополнительно</h4>
        <dl>
            <dt>Особенности</dt>
            <dd>
                <ul>
                    <li>Силовой кабель красный (25 мм / 5м)</li>
                    <li>Силовой кабель черный (25 мм / 1м)</li>
                    <li>RCA кабель зеленый (5 м)</li>
                    <li>Акустический кабель (3,5 мм / 10м)</li>
                    <li>Управляющий кабель зеленый (0,75 мм / 5,2м)</li>
                    <li>AGU корпус предохранителя</li>
                    <li>AGU предохранитель (60А)</li>
                    <li>Рельефная пластиковая трубка (1,8м)</li>
                </ul>
            </dd>
        </dl>
        <h4>Упаковка</h4>
        <dl>
            <dt>Размер упаковки (ВхШхД), см</dt>
            <dd>40 x 25 x 8 см, вес 1 кг</dd>
        </dl>
    </section>

</div>









