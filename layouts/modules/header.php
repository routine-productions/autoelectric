<div class="Header" xmlns="http://www.w3.org/1999/html">
    <div class="Header-Logo">
        <a href="/">
            <div class="Logo-Picture">
                <?php
                require_once "./img/Logo.svg"
                ?>
            </div>
            <div class="Header-Back">
                <svg>
                    <use xlink:href="#back"></use>
                </svg>
                <span>Вернуться на главную</span>
            </div>
        </a>
    </div>

    <div class="Header-Search">
        <div>
            <svg>
                <use xlink:href="#search"></use>
            </svg>
            <input type="text" placeholder="поиск по сайту...">
            <button>искать</button>
            <span class="Search-Example">
                например:
                <span>
                    Supra SCR-90R
                </span>
            </span>
        </div>
    </div>
    <div class="Header-Basket">
        <a href="basket" class="Basket-Logo">
            <svg>
                <use xlink:href="#basket"></use>
            </svg>
        </a>

        <div class="Header-Basket Info">
            <dl>
                <dt>в корзине:</dt>
                <dd>
                    <span class="Shopping-Basket">3</span> товара (<span class="Header-Price">8260.00</span>
                    <svg class="Rouble">
                        <use xlink:href="#rub"></use>
                    </svg>
                    )
                </dd>
            </dl>
            <button>
                <a href="basket">
                    Перейти в корзину
                </a>
            </button>
        </div>
    </div>

    <div class="Header-Mobile-Icons">
        <div class="Mobile-Basket">
            <a href="basket">
                <svg>
                    <use xlink:href="#basket"></use>
                </svg>
            </a>
        </div>
        <div class="Hamburger JS-Menu-Button">
            <span></span>
        </div>
    </div>


    <div class="Drobdown-Menu JS-Menu-List">
        <nav>
            <ul>
                <li><a href="">Автомагнитолы</a></li>
                <li><a href="">Автосигнализации</a></li>
                <li><a href="">Автохолодильники</a></li>
                <li><a href="">Антирадары</a></li>
                <li><a href="">Авторегистраторы</a></li>
                <li><a href="">GPS-навигаторы</a></li>
                <li><a href="">FM-модуляторы</a></li>
                <li><a href="">Камеры заднего вида</a></li>
                <li><a href="">Компрессоры</a></li>
                <li><a href="">Парктроники</a></li>
                <li><a href="">Полки и подиумы</a></li>
                <li><a href="">Пульты ДУ</a></li>
                <li><a href="">Телевизоры</a></li>
                <li><a href="">Усилители и аккустика</a></li>
                <li><a href="">Аксессуары и прочее</a></li>
            </ul>
        </nav>
    </div>

</div>