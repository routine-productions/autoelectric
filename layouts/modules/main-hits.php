<div class="Hits JS-Tabs">
    <nav class="Hits-Breadcrumbs JS-Tabs-Navigation">
        <div class="Hits-Menu">
            <svg class="Ribbon">
                <use xlink:href="#ribbon"></use>
            </svg>

            <span class="Tab-Result"></span>
            <div class="Hits-Select JS-Menu-List">
                <ul>
                    <li class="Active JS-Tab" data-href="Tab-1">Лидеры продаж</li>
                    <li class="JS-Tab" data-href="Tab-2">новые поступления</li>
                </ul>
            </div>

            <svg class="Drop-Down JS-Menu-Button">
                <use xlink:href="#arrow"></use>
            </svg>
        </div>

    </nav>
    <div class="JS-Tabs-Content">
        <div class="Hits-Items" data-tab='Tab-1'>

            <?php for ($index = 0; $index < 6; $index++) {
                require "./modules/item.php";
            }
            ?>
        </div>

        <div class="New-Items" data-tab='Tab-2'>

            <?php for ($index = 0; $index < 3; $index++) {
                require "./modules/item.php";
            }
            ?>
        </div>
    </div>

    <?php require "./modules/pagination.php"; ?>

</div>
