<div class="Slider JS-Carousel">

    <ul class="JS-Carousel-List">
        <li class="JS-Carousel-Item">
            <div class="Slide">
                <div class="Slide-View">
                    <div class="View"></div>
                </div>
                <div class="Slide-Content">
                    <h2>Prology CMU-500</h2>
                    <ul>
                        <li>Цифровой CD-проигрыватель</li>
                        <li>MP3-проигрыватель</li>
                        <li>DVD-проигрыватель</li>
                        <li>Blu-ray-проигрыватель</li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
