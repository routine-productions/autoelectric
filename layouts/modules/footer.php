<div class="Footer-Links Hide-Mobile">
    <div class="Text-Former">
        <ul class="Copyright">
            <li>Copyright © 2016</li>
            <li>Авто-электроника.рф</li>
        </ul>
        <div class="Made-In">
            <span>Сделано в </span>
            <a href="http://progress-time.ru">Progress Time</a>
        </div>
        <a href="" class="Vk">
            <svg>
                <use xlink:href="#vk"></use>
            </svg>
            <span>Группа ВКонтакте</span>
        </a>
    </div>
</div>
<div class="Additional">
    <section class="Text-Former">
        <h3>Дополнительно</h3>
        <ul>
            <li><a href="static">Производители</a></li>
            <li><a href="static">Подарочные сертификаты</a></li>
            <li><a href="static">Партнёрская программа</a></li>
            <li><a href="static">Акции</a></li>
        </ul>
    </section>

</div>
<div class="Support">
    <section class="Text-Former">
        <h3>Служба поддержки</h3>
        <ul>
            <li><a href="static">Связаться с нами</a></li>
            <li><a href="static">Возврат товара</a></li>
            <li><a href="static">Карта сайта</a></li>
        </ul>
    </section>
</div>
<div class="Caption"></div>
<div class="Footer-Info">
    <section class="Text-Former">
        <h3>Информация</h3>
        <ul>
            <li><a href="static">Шумоизоляция от "Стандартпласт"</a></li>
            <li><a href="static">Товары от "intro-incar"</a></li>
            <li><a href="static">Авто и мото запчасти на заказ</a></li>
        </ul>
    </section>
</div>
<div class="Footer-Links Hide-Desktop">
    <div class="Text-Former">
        <ul class="Copyright">
            <li>Copyright © 2016</li>
            <li>Авто-электроника.рф</li>
        </ul>
        <div class="Made-In">
            <span>Сделано в </span>
            <a href="http://progress-time.ru">Progress Time</a>
        </div>
        <a href="" class="Vk">
            <svg>
                <use xlink:href="#vk"></use>
            </svg>
            <span>Группа ВКонтакте</span>
        </a>
    </div>
</div>
