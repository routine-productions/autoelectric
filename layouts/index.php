<!DOCTYPE html>
<html>

<head lang="ru">
    <meta charset="UTF-8">
    <title>Автоэлектроника</title>

    <link
        href='https://fonts.googleapis.com/css?family=Open+Sans:400,300italic,300,400italic,600,600italic,700,700italic,800italic,800&subset=latin,cyrillic'
        rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="/css/index.min.css" type="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>

<body>


<?php
//error_reporting(E_ALL);
//require_once __DIR__ . '/libs/Mobile_Detect.php';
//$Detect = new Mobile_Detect;
//$Dir_Images = __DIR__ . '/images/';
//?>

<div class="Wrapper">
    <header>
        <?php require_once __DIR__ . "/modules/header.php"; ?>
    </header>

    <main>

        <?php
        $URI = explode('?',$_SERVER['REQUEST_URI'])[0];
        if ($URI == '/') {

            require __DIR__ . '/pages/main.php';

        } else {
            require __DIR__ . '/pages/' . $URI . '.php';
        }
        ?>
    </main>
    <footer>
        <?php require_once __DIR__ . "/modules/footer.php"; ?>
    </footer>
</div>

<?php require __DIR__ . "/img/sprite.svg"; ?>
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="index.min.js"></script>
<body>


</html>

