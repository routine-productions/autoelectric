<?php

class ControllerCheckoutConfirm extends Controller
{
    public function index()
    {
        $data = array();
        $this->load->model('checkout/order');
        error_reporting(E_ALL);

        // Main
        $data['store_id'] = $this->config->get('config_store_id');
        $data['store_name'] = $this->config->get('config_name');

        $data['firstname'] = $_POST['basket-fio'];
        $data['email'] = $_POST['basket-email'];
        $data['telephone'] = $_POST['basket-phone'];
        $data['comment'] = $_POST['basket-comment'];
        $data['shipping_method'] = $_POST['method'];

        // Products
        foreach ($this->cart->getProducts() as $product) {
            $product_data[] = array(
                'product_id' => $product['product_id'],
                'name' => $product['name'],
                'model' => $product['model'],
                'download' => $product['download'],
                'quantity' => $product['quantity'],
                'subtract' => $product['subtract'],
                'price' => $product['price'],
                'total' => $product['total'],
                'tax' => 0,
                'reward' => $product['reward']
            );
        }
        $data['products'] = $product_data;

        // Total
        $total_data = array();
        $total = 0;
        $taxes = $this->cart->getTaxes();

        $this->load->model('setting/extension');

        $sort_order = array();

        $results = $this->model_setting_extension->getExtensions('total');

        foreach ($results as $key => $value) {
            $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
        }

        array_multisort($sort_order, SORT_ASC, $results);

        foreach ($results as $result) {
            if ($this->config->get($result['code'] . '_status')) {
                $this->load->model('total/' . $result['code']);

                $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
            }
        }

        $data['totals'] = $total_data;
        $data['total'] = $total;

        // Add
        $this->session->data['order_id'] = $this->model_checkout_order->addOrder($data);

        // Clear
        $this->cart->clear();

        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header'
        );

        $this->data = [
            'order_id' => $this->session->data['order_id'],
            'basket_fio' => $_POST['basket-fio'],
            'basket_email' => $_POST['basket-email'],
            'basket_phone' => $_POST['basket-phone'],
            'method' => $_POST['method'],
            'totals' => $total_data,
            'total' => $total
        ];

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/confirm.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/checkout/confirm.tpl';
        } else {
            $this->template = 'default/template/checkout/confirm.tpl';
        }

        $this->response->setOutput($this->render());
    }
}