<?php echo $header; ?>

<div class="Middle"></div>

<div class="Right-Content">
    <div class="Main-Wrap">
        <div class="Hits">
            <nav class="Hits-Breadcrumbs">
                <h2 class="Tab-Header"><?= $heading_title; ?></h2>
            </nav>
            <div class="JS-Tabs-Content">
                <div class="Message-Error"><?php echo $text_error; ?></div>
            </div>
        </div>
    </div>
</div>

<?php echo $footer; ?>