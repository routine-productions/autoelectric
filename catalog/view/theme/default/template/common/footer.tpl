</main>
<footer>
    <div class="Footer-Links Hide-Mobile">
        <div class="Text-Former">

            <ul class="Copyright">
                <li>Copyright © 2016</li>
                <li>Авто-электроника.рф</li>
            </ul>

            <div class="Made-In">
                <span>Сделано в </span>
                <a href="http://progress-time.ru/">Progress Time</a>
            </div>

            <a href="http://vk.com/" class="Vk">
                <svg>
                    <use xlink:href="#vk"></use>
                </svg>
                <span>Группа ВКонтакте</span>
            </a>

        </div>
    </div>

    <div class="Additional">
        <section class="Text-Former">
            <h3>Дополнительно</h3>
            <ul>
                <li><a href="/index.php?route=information/information&information_id=4">О компании</a></li>
                <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
                <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
            </ul>
        </section>
    </div>

    <div class="Support">
        <section class="Text-Former">
            <h3>Информация</h3>
            <ul>

                <li><a href="http://auto-electronica/index.php?route=information/information&amp;information_id=9">Товары от &quot;intro-incar&quot;</a></li>
                <li><a href="http://auto-electronica/index.php?route=information/information&amp;information_id=8">Авто и мото запчасти на заказ</a></li>
                <li><a href="http://auto-electronica/index.php?route=information/information&amp;information_id=11">Изготовление сабвуферов</a></li>
                <li><a href="http://auto-electronica/index.php?route=information/information&amp;information_id=6">Оклейка виниловыми пленками</a></li>
            </ul>
        </section>
    </div>
      <div class="Caption"></div>




    <div class="Footer-Info">
        <section class="Text-Former ">
            <h3>ПОМОЩЬ</h3>
            <ul>
                <li><a href="http://auto-electronica/index.php?route=information/information&amp;information_id=10">Шумоизоляция от &quot;Стандартпласт&quot;</a></li>
                <li><a href="http://auto-electronica/index.php?route=information/information&amp;information_id=5">Помощь в выборе автомобиля</a></li>
                <li><a href="http://auto-electronica/index.php?route=information/information&amp;information_id=7">Диагностика электронных систем</a></li>
            </ul>
        </section>
    </div>

    <div class="Footer-Links Hide-Desktop">
        <div class="Text-Former">
            <ul class="Copyright">
                <li>Copyright © 2016</li>
                <li>Авто-электроника.рф</li>
            </ul>
            <div class="Made-In">
                <span>Сделано в </span>
                <a href="http://progress-time.ru">Progress Time</a>
            </div>
            <a href="" class="Vk">
                <svg>
                    <use xlink:href="#vk"></use>
                </svg>
                <span>Группа ВКонтакте</span>
            </a>
        </div>
    </div>

</footer>

</div>

<?php require DIR_ROOT . "img/sprite.svg"; ?>

<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>

<script src="index.min.js"></script>

<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>

<script type="text/javascript" src="catalog/view/javascript/common.js"></script>

<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>

<?php if ($stores) { ?>
<script type="text/javascript"><!--
    $(document).ready(function () {
        <?php foreach ($stores as $store) { ?>
            $('body').prepend('<iframe src="<?php echo $store; ?>" style="display: none;"></iframe>');
            <?php } ?>
        });
    //--></script>
<?php } ?>

</body></html>
