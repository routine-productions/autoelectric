<?php echo $header; ?>

<div class="Middle"></div>
<div class="Right-Content">
    <div class="Main-Wrap">
        <?php echo $column_left; ?>
        <?php echo $column_right; ?>

        <div class="Hits JS-Tabs">
            <nav class="Hits-Breadcrumbs JS-Tabs-Navigation">
                <div class="Hits-Menu">
                    <svg class="Ribbon">
                        <use xlink:href="#ribbon"></use>
                    </svg>

                    <span class="Tab-Result"></span>
                    <div class="Hits-Select JS-Menu-List">
                        <ul>
                            <li class="Active JS-Tab" data-href="Tab-1">Лидеры продаж</li>
                            <li class="JS-Tab" data-href="Tab-2">Новые поступления</li>
                        </ul>
                    </div>

                    <svg class="Drop-Down JS-Menu-Button">
                        <use xlink:href="#arrow"></use>
                    </svg>
                </div>
            </nav>
            <div class="JS-Tabs-Content">
                <?php echo $content_top; ?>
            </div>
        </div>

        <?php echo $content_bottom; ?>
    </div>
</div>

<?php echo $footer; ?>