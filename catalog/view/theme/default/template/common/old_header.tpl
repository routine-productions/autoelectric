﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8"/>
    <title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>

    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " -
    ". ((int) $_GET['page'])." ".$text_page;} ?>" />
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content="<?php echo $keywords; ?>"/>
    <?php } ?>
    <meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " -
    ". ((int) $_GET['page'])." ".$text_page;} ?>" />
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="<?php echo $og_url; ?>"/>
    <?php if ($og_image) { ?>
    <meta property="og:image" content="<?php echo $og_image; ?>"/>
    <?php } else { ?>
    <meta property="og:image" content="<?php echo $logo; ?>"/>
    <?php } ?>
    <meta property="og:site_name" content="<?php echo $name; ?>"/>
    <?php if ($icon) { ?>
    <link href="<?php echo $icon; ?>" rel="icon"/>
    <?php } ?>

    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>"/>
    <?php } ?>

    <?php foreach ($styles as $style) { ?>
    <link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>"
          media="<?php echo $style['media']; ?>"/>
    <?php } ?>

    <?php echo $google_analytics; ?>

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300italic,300,400italic,600,600italic,700,700italic,800italic,800&subset=latin,cyrillic'
          rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="/css/index.min.css" type="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>

<div class="Wrapper">
    <header>
        <div class="Header">

            <div class="Header-Logo">
                <a href="/">
                    <div class="Logo-Picture">
                        <?php
                require_once "./img/Logo.svg"
                ?>
                    </div>
                    <div class="Header-Back">
                        <svg>
                            <use xlink:href="#back"></use>
                        </svg>
                        <span>Вернуться на главную</span>
                    </div>
                </a>
            </div>

            <div class="Header-Search">
                <div>
                    <svg>
                        <use xlink:href="#search"></use>
                    </svg>
                    <input type="text" placeholder="поиск по сайту..." value="<?php echo $search; ?>">
                    <button>искать</button>
                    <span class="Search-Example">
                        например:
                        <span>
                            Supra SCR-90R
                        </span>
                    </span>
                </div>
            </div>

            <div class="Header-Basket">
                <a href="/index.php?route=checkout/cart" class="Basket-Logo">
                    <svg>
                        <use xlink:href="#basket"></use>
                    </svg>
                </a>

                <div class="Header-Basket Info">
                    <dl>
                        <dt>в корзине:</dt>

                        <?php echo $cart; ?>

                    </dl>
                    <button>
                        <a href="/index.php?route=checkout/cart">
                            Перейти в корзину
                        </a>
                    </button>
                </div>
            </div>

            <div class="Header-Mobile-Icons">
                <div class="Mobile-Basket">
                    <a href="/index.php?route=checkout/cart">
                        <svg>
                            <use xlink:href="#basket"></use>
                        </svg>
                    </a>
                </div>
                <div class="Hamburger JS-Menu-Button">
                    <span></span>
                </div>
            </div>

            <div class="Drobdown-Menu JS-Menu-List">
                <nav>
                    <ul>
                        <?php foreach($categories[0]['children'] as $category){ ?>
                        <li><a href="<?=$category['href']?>"><?=$category['name']?></a></li>
                        <?php }?>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <main>
        <div class="Left-Content">
            <nav class="Main-Menu">
                <h2>
                    <svg>
                        <use xlink:href="#categories"></use>
                    </svg>
                    Категории
                </h2>
                <?php foreach($categories[0]['children'] as $category){ ?>
                <a href="<?=$category['href']?>"><?=$category['name']?></a>
                <?php }?>
            </nav>
        </div>
