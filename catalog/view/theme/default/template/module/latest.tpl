<div class="New-Items" data-tab='Tab-2'>
    <?php foreach ($products as $product) { ?>
    <?php //print_r($product);exit;?>

    <div class="Item">
        <a href="<?php echo $product['href'];?>" class="Item-View"
           style="background-image: url(<?php echo $product['thumb'];?>)"></a>
        <section class="Item-Content">
            <a href="<?php echo $product['href'];?>">
                <h3 class="Item-Title"><?php echo $product['name'];?></h3>
            </a>

            <?php echo html_entity_decode($product['description']);?>
        </section>
        <div class="Item-Price">
            <span><?php echo $product['price'];?>
                <svg>
                    <use xlink:href="#rub"></use>
                </svg></span>
        </div>
        <div class="Item-Button">
            <button value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product[" product_id
            "]; ?>');">Купить</button>
        </div>
    </div>
    <?php } ?>
</div>
