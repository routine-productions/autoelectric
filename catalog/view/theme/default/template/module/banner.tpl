<div class="Slider JS-Carousel">
    <ul class="JS-Carousel-List">
        <?php foreach($banners as $banner){ ?>
        <li class="JS-Carousel-Item">
            <a href="<?=$banner['link']?>" class="Slide">
                <div class="Slide-View">
                    <div class="View" style="background-image: url(<?=$banner['image']?>);"></div>
                </div>
                <div class="Slide-Content">
                    <h2><?=$banner['title']?></h2>
                    <?=$banner['description']?>
                </div>
            </a>
        </li>
        <?php }?>
    </ul>
</div>
