<?php echo $header; ?>

<div class="Middle"></div>
<div class="Right-Content Mod">
    <section class="Static">

        <h2><?php echo $heading_title; ?></h2>
        <article>
            <?php echo $description; ?>
        </article>

        <?php echo $content_bottom; ?>
    </section>
</div>

<?php echo $footer; ?>