<?php echo $header; ?>

<div class="Middle" xmlns="http://www.w3.org/1999/html"></div>

<div class="Right-Content">
    <div class="Main-Wrap">
        <div class="Hits">

            <nav class="Hits-Breadcrumbs">
                <h2 class="Tab-Header"><?= $heading_title; ?></h2>
            </nav>

            <form class='Contacts' action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

                <ul>
                    <li>
                        <label><?php echo $entry_name; ?></label>
                        <input type="text" name="name" value="<?php echo $name; ?>"/>
                        <?php if ($error_name) { ?>
                        <span class="error"><?php echo $error_name; ?></span>
                        <?php } ?>
                    </li>

                    <li>
                        <label><?php echo $entry_email; ?></label>
                        <input type="text" name="email" value="<?php echo $email; ?>"/>

                        <?php if ($error_email) { ?>
                        <span class="error"><?php echo $error_email; ?></span>
                        <?php } ?>
                    </li>

                    <li>
                        <label><?php echo $entry_enquiry; ?></label>
                        <textarea name="enquiry" cols="40" rows="10"
                                  style="width: 99%;"><?php echo $enquiry; ?></textarea>

                        <?php if ($error_enquiry) { ?>
                        <span class="error"><?php echo $error_enquiry; ?></span>
                        <?php } ?>
                    </li>
                </ul>
                <button>Отправить</button>
            </form>

            <div class="Contacts">
                <h2><?php echo $text_location; ?></h2>

                <p><b><?php echo $text_address; ?></b> <?php echo $store; ?> <?php echo $address; ?></p>

                <p><b><?php echo $text_telephone; ?></b> <?php echo $telephone; ?></p>

            </div>
        </div>
    </div>
</div>

<?php echo $footer; ?>