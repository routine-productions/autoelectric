<?php echo $header; ?>

<div class="Middle"></div>

<div class="Right-Content">
    <div class="Main-Wrap Hits">

        <nav class="Hits-Breadcrumbs">
            <h2 class="Tab-Header"><?= $heading_title; ?></h2>
        </nav>

        <div class="Sitemap">
            <ul>
                <?php foreach ($categories as $category_1) { ?>
                <li><a href="<?php echo $category_1['href']; ?>"><?php echo $category_1['name']; ?></a>
                    <?php if ($category_1['children']) { ?>
                    <ul>
                        <?php foreach ($category_1['children'] as $category_2) { ?>
                        <li><a href="<?php echo $category_2['href']; ?>"><?php echo $category_2['name']; ?></a>
                            <?php if ($category_2['children']) { ?>
                            <ul>
                                <?php foreach ($category_2['children'] as $category_3) { ?>
                                <li><a href="<?php echo $category_3['href']; ?>"><?php echo $category_3['name']; ?></a>
                                </li>
                                <?php } ?>
                            </ul>
                            <?php } ?>
                        </li>
                        <?php } ?>
                    </ul>
                    <?php } ?>
                </li>
                <?php } ?>

            </ul>
        </div>

    </div>
</div>

<?php echo $footer; ?>