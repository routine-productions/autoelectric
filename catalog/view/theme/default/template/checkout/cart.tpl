<?php echo $header; ?>

<div class="Middle"></div>
<div class="Right-Content Mod">
    <section class="Basket">

        <h2>
            <svg>
                <use xlink:href="#basket"></use>
            </svg>
            Корзина
        </h2>

        <div class="Basket-Next">Далее</div>
        <div class="Basket-Previous">Назад</div>

        <section class="Basket-Items Active">
            <form action="">
                <ul>
                    <?php foreach ($products as $key => $product) { ?>
                    <li class="Basket-Item">
                        <span class="Basket-Number">0<?=$key+1?></span>

                        <div class="Basket-Picture">
                            <img src="<?=$product['thumb']?>" alt="">
                        </div>
                        <span class="Basket-Name"><?=$product['name']?></span>
                     <span class="Basket-Price"><?=$product['price']?>
                         <svg class="Rouble">
                             <use xlink:href="#rub"></use>
                         </svg>
                    </span>

                        <div class="Basket-Count">
                            <div class="Previous JS-Click-Left">
                                <svg>
                                    <use xlink:href="#arrow"></use>
                                </svg>
                            </div>
                            <div class="Basket-Amount">
                                <span><?= $product['quantity']?></span>
                                <input type="hidden" name="quantity[<?php echo $product['key']; ?>]"
                                       value="<?php echo $product['quantity']; ?>" size="1">
                            </div>
                            <div class="Next JS-Click-Right">
                                <svg>
                                    <use xlink:href="#arrow"></use>
                                </svg>
                            </div>
                        </div>
                        <div class="Basket-Close" href="<?php echo $product['remove']; ?>">
                            <svg>
                                <use xlink:href="#cross"></use>
                            </svg>
                        </div>
                    </li>
                    <?php }?>

                </ul>
            </form>
            <span class="All-Price">Итого:
                <span class="JS-All-Price"><?=$totals[1]['value']?></span>
                 <svg class="Rouble">
                     <use xlink:href="#rub"></use>
                 </svg>
            </span>
        </section>


        <form action="/index.php?route=checkout/confirm" method="post">
            <section class="Basket-Method">
                <h2>Выберите способ доставки</h2>

                <ul>
                    <li class="Method-Item">
                        <div class="Method-Choice">
                            <input type="radio" name="method" value="Доставка курьером по предоплате (г. Владимир и Владимирская область)" id="method-1" checked>
                            <svg>
                                <use xlink:href="#checkbox"></use>
                            </svg>
                            <label for="method-1"></label>
                        </div>
                        <span class="Method-Number">01</span>

                        <div class="Method-Description">
                            <h3>Доставка курьером по предоплате</h3>
                            <span>г. Владимир и Владимирская область<span> (100% предоплата)</span></span>
                        </div>
                        <div class="Method-Time">
                            <span>3-5 дней</span>
                        </div>
                    </li>
                    <li class="Method-Item">
                        <div class="Method-Choice">
                            <input type="radio" name="method" value="Доставка Транспортной компанией (вся Россия)" id="method-2">
                            <svg>
                                <use xlink:href="#checkbox"></use>
                            </svg>
                            <label for="method-2"></label>
                        </div>
                        <span class="Method-Number">02</span>

                        <div class="Method-Description">
                            <h3>Доставка Транспортной компанией</h3>
                            <span>вся Россия<span> (100% предоплата)</span></span>
                        </div>
                        <div class="Method-Time">
                            <span>3-5 дней</span>
                            <span class="Time-Plus">+ Сроки транспортной компании</span>
                        </div>
                    </li>
                    <li class="Method-Item Himself">
                        <div class="Method-Choice">
                            <input type="radio" name="method" value="Самовывоз г. Владимир" id="method-3">
                            <svg>
                                <use xlink:href="#checkbox"></use>
                            </svg>
                            <label for="method-3"></label>
                        </div>
                        <span class="Method-Number">03</span>

                        <div class="Method-Description">
                            <h3>Самовывоз</h3>
                            <span>г. Владимир<span> (50% предоплата)</span></span>
                        </div>
                        <div class="Method-Time">
                            <span>3-5 дней</span>
                        </div>
                    </li>
                </ul>
            </section>

            <section class="Basket-Contacts">
                <h2>Ваши контактные данные</h2>
                <ul>
                    <li>
                        <label for="basket-fio">Ф.И.О.</label>
                        <input type="text" id="basket-fio" name="basket-fio">
                    </li>
                    <li>
                        <label for="basket-phone">Телефон</label>
                        <input type="text" id="basket-phone" name="basket-phone">
                    </li>
                    <li class="Basket-Address">
                        <label for="basket-address">Адрес доставки</label>
                        <input type="text" id="basket-address" name="basket-address">
                    </li>
                    <li class="Basket-Comment">
                        <label for="basket-comment">Комментарий к доставке</label>
                        <textarea id="basket-comment"></textarea>
                    </li>
                </ul>

                <button>Перейти к оплате</button>
            </section>
        </form>
    </section>
</div>

<?php echo $footer; ?>