<?php echo $header; ?>

<div class="Middle"></div>
<div class="Right-Content Mod">
    <section class="Basket">

        <h2>
            <svg>
                <use xlink:href="#basket"></use>
            </svg>
            Корзина
        </h2>

        <p>Спасибо за заказ!</p>
        <br>
        <p>Вы можете совершить оплату у нас на сайте.</p>
        <br>

        <iframe frameborder="0" allowtransparency="true" scrolling="no"
                src="https://money.yandex.ru/embed/shop.xml?account=410012866966713&quickpay=shop&payment-type-choice=on&mobile-payment-type-choice=on&writer=seller&targets=Оплата заказа №<?= $order_id; ?> от <?= $basket_fio; ?>&targets-hint=&default-sum=<?=$totals[0]['value']?>&button-text=01&fio=on&mail=on&phone=on&address=on&successURL=%D0%B0%D0%B2%D1%82%D0%BE-%D1%8D%D0%BB%D0%B5%D0%BA%D1%82%D1%80%D0%BE%D0%BD%D0%B8%D0%BA%D0%B024.%D1%80%D1%84" width="450" height="198"></iframe>
    </section>
</div>

<?php echo $footer; ?>