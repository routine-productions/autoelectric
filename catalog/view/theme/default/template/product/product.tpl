<?php echo $header; ?>

<div class="Middle"></div>

<div class="Right-Content Mog-bg">
    <nav class="Thing-Nav">
        <div class="Thing-Breadcrumbs">
            <a href='/index.php?route=product/category&path=<?=$category_id?>'>
                <span>
                    <svg>
                        <use xlink:href="#back"></use>
                    </svg>вернуться в категорию <span><?php print_r($category_title); ?></span>
                 </span>
            </a>
        </div>
        <div class="Thing-Price">
            <span>Цена:</span>
            <span><?=$price ?>
                <svg>
                    <use xlink:href="#rub"></use>
                </svg>
            </span>
        </div>
    </nav>
    <div class="Thing-Content">
        <div class="Thing-View"></div>
        <section class="Thing-Description">
            <h2><?=$heading_title?></h2>
            <dl>
                <dd>Производитель:</dd>
                <dt><?= $manufacturer; ?></dt>
            </dl>
            <dl>
                <dd>Модель:</dd>
                <dt><?php echo $model; ?></dt>
            </dl>
            <dl>
                <dd>Наличие:</dd>
                <dt><?=$stock?></dt>
            </dl>

            <button onclick="addToCart('<?= $product_id; ?>');">В корзину</button>
        </section>
    </div>
    <section class="Thing-Info">
        <h3>Характеристики</h3>
        <dl>
            <dt>Производитель</dt>
            <dd><?= $manufacturer; ?></dd>
        </dl>
        <dl>
            <dt>Модель</dt>
            <dd><?php echo $model; ?></dd>
        </dl>

        <dl>
            <?php echo $description; ?>
        </dl>

    </section>

    <?php /// echo $column_left; ?>
    <?php echo $column_right; ?>


    <?php echo $content_bottom; ?>
</div>


<?php echo $footer; ?>