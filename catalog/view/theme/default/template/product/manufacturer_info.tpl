<?php echo $header; ?>

<div class="Middle"></div>
<div class="Right-Content">
    <div class="Main-Wrap">
        <div class="Hits">
            <nav class="Hits-Breadcrumbs">
                <h2 class="Tab-Header"><?= $heading_title; ?></h2>
            </nav>
            <div class="Hits-Items">
                <?php if ($products) { ?>
                <?php foreach ($products as $product) { ?>
                <div class="Item">
                    <a href="<?php echo $product['href'];?>" class="Item-View"
                       style="background-image: url(<?php echo $product['thumb'];?>)"></a>
                    <section class="Item-Content">
                        <a href="<?php echo $product['href'];?>">
                            <h3 class="Item-Title"><?php echo $product['name'];?></h3>
                        </a>
                        <p>
                            <?php echo html_entity_decode($product['description']);?>
                        </p>
                    </section>
                    <div class="Item-Price">
                        <span><?php echo $product['price'];?>
                            <svg>
                                <use xlink:href="#rub"></use>
                            </svg></span>
                    </div>
                    <div class="Item-Button">
                        <button value="<?php echo $button_cart; ?>"
                                onclick="addToCart('<?php echo $product['product_id']; ?>');">Купить
                        </button>
                    </div>
                </div>
                <?php } ?>
                <?php } ?>
            </div>
            <?php if($pagination){ ?>
            <div class="Pagination"><?php echo $pagination; ?></div>
            <?php }?>
        </div>


        <?php echo $content_bottom; ?>
    </div>
</div>

<?php echo $footer; ?>