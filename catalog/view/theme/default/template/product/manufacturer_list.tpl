<?php echo $header; ?>

<div class="Middle"></div>

<div class="Right-Content">
    <div class="Main-Wrap Hits">

        <nav class="Hits-Breadcrumbs">
            <h2 class="Tab-Header"><?= $heading_title; ?></h2>
        </nav>
        <div class="Manufacturers">
            <?php foreach ($categories as $category) { ?>

            <div class="Manufacturer-List">
                <div class="Manufacturer-Letter"><?= $category['name']; ?></div>
                <?php foreach ($category['manufacturer'] as $Manufacturer) { ?>
                     <div><a href="<?= $Manufacturer['href']; ?>"><?= $Manufacturer['name']; ?></a></div>
                <?php } ?>
            </div>
            <?php } ?>
        </div>
    </div>
</div>

<?php echo $footer; ?>