$('.Method-Item label').click(function(){
    if($(this).parents('.Method-Item').hasClass("Himself")){
        $('.Basket-Address, .Basket-Comment').addClass('Removed');
    }else{
        $('.Basket-Address, .Basket-Comment').removeClass('Removed');
    }
});


$(".Basket-Next").click(function(){
    if($(".Basket section.Active").next().is('section')){
        $(".Basket section.Active").removeClass("Active").next().addClass("Active");
    }else{
        return false;
    }
});


$(".Basket-Previous").click(function(){
    if($(".Basket section.Active").prev().is('section')){
        $(".Basket section.Active").removeClass("Active").prev().addClass("Active");
    }else{
        return false;
    }
});