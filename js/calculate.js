var Index;
var One_Price;
var Suma;


function Sum() {
   Suma = 0;
    $(".Basket-Item").each(function(){

        One_Price = parseInt($(this).find('.Basket-Price').text()) * parseInt($(this).find('.Basket-Amount span').text());

        Suma = One_Price + Suma;
        console.log(parseInt($(this).find('.Basket-Price').text()));
        console.log(parseInt($(this).find('.Basket-Amount span').text()));
        console.log(Suma);

        $('.JS-All-Price').text(Suma);
    });

    $('.JS-All-Price').text(Suma);
}

function Index_Minus(I) {
    if (I > 1) {
        Index--;
    }else{
        Index = 1;
    }
}

function Index_Plus(I) {
    if (I < 1000) {
        Index++;
    }else{
        Index = 1000;
    }
}


function Prepay(){
    if(Suma > 15000){
        $('.Method-Description span span').css("display","none");
    }else{
        $('.Method-Description span span').css("display","");
    }
}


$('.JS-Click-Left').click(function () {

    Index = parseInt($(this).parents('.Basket-Count').find("span").text());

    Index_Minus(Index);

    $(this).parents('.Basket-Count').find("span").text(Index);
    $(this).parents('.Basket-Count').find("input").val(Index);

    Sum();
    Prepay();

    $.ajax({
        url : 'index.php?route=checkout/cart',
        type : 'POST',
        data : $('.Basket-Items form').serialize()
    });

    return false;
});

$('.JS-Click-Right').click(function () {

    Index = parseInt($(this).parents('.Basket-Count').find("span").text());

    Index_Plus(Index);

    $(this).parents('.Basket-Count').find("span").text(Index);
    $(this).parents('.Basket-Count').find("input").val(Index);

    Sum();
    Prepay();


    $.ajax({
        url : 'index.php?route=checkout/cart',
        type : 'POST',
        data : $('.Basket-Items form').serialize()
    });

    return false;

});


if($('.Basket-Item').size() == 0){
    $('.Basket-Items ul').text('У вас нет выбранных товаров');

    Sum();
}

$(".Basket-Close svg").click(function(){
    $.ajax($(this).parents(".Basket-Close").attr('href'));
    $(this).parents(".Basket-Item").remove();


    Sum();
    Prepay();

    if($('.Basket-Item').size() == 0){
        $('.Basket-Items ul').text('У вас нет выбранных товаров');
    }
    return false;
});