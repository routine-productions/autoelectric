/*
 * Copyright (c) 2015
 * Routine JS - Menu
 * Version 0.1.0
 * Create 2015.12.18
 * Author Bunker Labs

 * Usage:
 * 1)add structure
 * 2)add necessarily style
 * 3)set number value attribute 'data-height-menu'


 * Code structure:
 *  <div class="Script-Menu">
 *       <div class="Menu-Button" data-height-menu="6">
 *          <span>Click Here</span>
 *       </div>
 *       <div class="Menu-List">
 *           <ul>
 *               <li>Item 1</li>
 *               <li>Item 2</li>
 *               <li>Item 3</li>
 *               <li>Item 4</li>
 *               <li>Item 5</li>
 *               <li>Item 6</li>
 *               <li>Item 7</li>
 *               <li>Item 8</li>
 *               <li>Item 9</li>
 *               <li>Item 10</li>
 *           </ul>
 *       </div>
 *   </div>


 * Necessarily css
 *
 *   .Menu-List{
 *        display: none;
 *        height: 0;
 *        overflow-y: scroll;
 *        transition: height 0.2s linear;
 *    }
 *

 */


$.fn.Drobdown_Header = function () {
    var $Global = $(this);


    $Global.find('.JS-Menu-Button').click(function () {

        if ($Global.find('.JS-Menu-List').is(':hidden')) {

            var Actual_Height = $Global.find('.JS-Menu-List ul').actual('outerHeight');

            $Global.find('.JS-Menu-List').css({'display': 'block', 'height': Actual_Height});

        } else {
            function Time_Display() {
                $Global.find('.JS-Menu-List').css({'display': 'none'});
            }

            $Global.find('.JS-Menu-List').css({'height': '0'});

            setTimeout(Time_Display, 200);
        }

        return false;
    });


    $('body, html').click(function () {

        $Global.find('.JS-Menu-List').css({'height': '0'});

        function Time_Display_Body() {
            $Global.find('.JS-Menu-List').css({'display': 'none'});
        }

        setTimeout(Time_Display_Body, 200);


    });

};


var Width = $(window).width();
//
$(window).load(function () {
    if (Width >= 601) {

        $(".Hits-Select").removeClass('JS-Menu-List');
    } else {
        $(".Hits-Select").addClass('JS-Menu-List');

    }

});


$(window).resize(function () {

    Width = $(window).width();

    if (Width <=600) {

        $(".Drop-Down.JS-Menu-Button, .Hits-Menu").removeClass('Active');
        $(".Hits-Select").css({"display": "none", "height": "0"});
        $(".Hits-Select").addClass('JS-Menu-List');

    } else if (Width >= 601) {
        $(".Hits-Select").css({"display": "inline-block", "height": "auto"});
        $(".Hits-Select").removeClass('JS-Menu-List');
    }

    if (Width >= 1200) {

        $('.Drobdown-Menu').css({"display": "none", "height": "0"});
    }


});


$.fn.Drobdown_Category = function () {

    var $Global = $(this);

    $Global.find('.JS-Menu-Button').click(function () {

        if ($Global.find('.JS-Menu-List').is(':hidden')) {

            var Actual_Height = $Global.find('.JS-Menu-List ul').actual('outerHeight');

            $Global.find('.JS-Menu-List').css({'display': 'inline-block', 'height': Actual_Height});

            $('.Hits-Menu').addClass("Active");
            $('.Drop-Down').addClass("Active");

        } else {
            function Time_Display() {
                $Global.find('.JS-Menu-List').css({'display': 'none'});
                $('.Hits-Menu').removeClass("Active");

            }


            $Global.find('.JS-Menu-List').css({'height': '0'});
            $('.Drop-Down').removeClass("Active");

            setTimeout(Time_Display, 200);

        }

        return false;
    });


    $(document).find($Global).find('.Hits-Select').on('click', 'li', function () {

        //tabs start

        var Hash = $(this).attr('data-href');
        $(this).parents('.JS-Tabs-Navigation').find('.JS-Tab').removeClass('Active');

        $(this).parents('.JS-Tabs').find('.JS-Tabs-Content [data-tab=' + Hash + ']').show().siblings('[data-tab !=' + Hash + ']').hide();
        $(this).addClass('Active');

        history.pushState(null, null, "#" + Hash);

        //tabs end


        var Text = $(this).text();

        $('.Hits-Breadcrumbs').find('.Tab-Result').text(Text);

        $Global.find('.JS-Menu-Button').trigger('click');

    });


    $('body, html').click(function () {

        $Global.find('.JS-Menu-List').css({'height': '0'});

        function Time_Display_Body() {
            $Global.find('.JS-Menu-List').css({'display': 'none'});
        }

        setTimeout(Time_Display_Body, 200);


    });


};


$(".Hits-Breadcrumbs").Drobdown_Category();

$(".Header").Drobdown_Header();



