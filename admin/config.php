<?php
// HTTP
define('HTTP_SERVER', 'http://' . $_SERVER['SERVER_NAME'] . '/admin/');
define('HTTP_CATALOG', 'http://' . $_SERVER['SERVER_NAME'] . '/');

// HTTPS
define('HTTPS_SERVER', 'http://' . $_SERVER['SERVER_NAME'] . '/admin/');
define('HTTPS_CATALOG', 'http://' . $_SERVER['SERVER_NAME'] . '/');

// DIR
define('DIR_APPLICATION', __DIR__ . '/../admin/');
define('DIR_SYSTEM', __DIR__ . '/../system/');
define('DIR_DATABASE', __DIR__ . '/../system/database/');
define('DIR_LANGUAGE', __DIR__ . '/../admin/language/');
define('DIR_TEMPLATE', __DIR__ . '/../admin/view/template/');
define('DIR_CONFIG', __DIR__ . '/../system/config/');
define('DIR_IMAGE', __DIR__ . '/../image/');
define('DIR_CACHE', __DIR__ . '/../system/cache/');
define('DIR_DOWNLOAD', __DIR__ . '/../download/');
define('DIR_LOGS', __DIR__ . '/../system/logs/');
define('DIR_CATALOG', __DIR__ . '/../catalog/');

// DB
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '987975');
define('DB_DATABASE', 'auto-electronics');
define('DB_PREFIX', 'p62f0_');
?>
